<?php

require 'vendor/autoload.php';
Handlebars\Autoloader::register();

use Handlebars\Handlebars;
use Handlebars\Loader\FilesystemLoader;

$loader = new FilesystemLoader(__DIR__ . '/views/', [
  'extension' => 'hbs',
  'prefix' => ''
]);

$hbs = new Handlebars([
  'loader' => $loader,
  'partials_loader' => $loader
]);

echo $hbs->render(
  'main', [
    'custom' => [
      'date' => date('Y.m.d.'),
      'time' => date('H:i'),
      'sunrise' => '00:00',
      'sunset' => '00:00'
    ],
    'humindex' => 0,
    'humid' => 0,
    'temp' => '-0',
    'wind' => 0,
    'time' => '00:00:00'
  ]
);
