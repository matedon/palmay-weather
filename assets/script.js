'use strict'
;
(function ($) {
  const $widget = $(window.widget)
  const requestSunset = function (cb) {
    $.ajax({
      'url': 'http://api.sunrise-sunset.org/json',
      'type': 'GET',
      'dataType': 'json',
      'data': {
        'lat': 47.102809,
        'lng': 17.909302
      }
    }).always(cb)
  }
  const renderHandler = function (template, data) {
    $widget.html(Handlebars.compile(template)(data))
    setTimeout(function () {
      $widget.find('.fa-spin').removeClass('fa-spin')
    }, 1000)
  }
  const responsePatcher = function (data, sun, battery) {
    $.each(data, function (key, val) {
      const regex = /<img.*?src="(.*?)"/
      const src = regex.exec(val)
      if (src && src.length == 2) {
        data[key] = 'http://m.palmay.hu/' + src[1]
      }
    })
    data.custom = {
      weather: data.weather.replace(/(<([^>]+)>)/ig, '\n').split('\n'),
      sunset: Date.parse(sun.results.sunset).addHours(1).toString('HH:mm'),
      sunrise: Date.parse(sun.results.sunrise).addHours(1).toString('HH:mm'),
      humidtrend: data.humidtrend ? (data.humidtrend.indexOf('down') === -1 ? 'up' : 'down') : null,
      battery: false
    }
    if (battery) {
      data.custom.battery = {
        isLow: battery.percent < 15,
        isFull: battery.percent > 95,
        charging: battery.charging,
        percent: battery.percent,
        quater: Math.round(battery.level * 4)
      }
    }
    $.extend(data.custom, getCustomDate())
    return data
  }
  const responseToJSON = function (res) {
    const pairs = res.split('||')
    const result = {}
    $.each(pairs, function (index, data) {
      const keyVal = data.split('==')
      let key
      let val
      if (keyVal.length == 1) {
        key = index
        val = data
      } else {
        key = keyVal[0]
        val = keyVal[1]
      }
      result[key] = val
    })
    return result
  }
  const weatherRequest = function (cb) {
    // Bypass Corss Origin with onlinecurl
    $.ajax({
      'url': 'http://curl.matedon.com',
      'type': 'POST',
      'data': {
        'key': '880bd918c23436068b085ff7c5070792',
        'url': 'http://m.palmay.hu/wth.php'
      }
    }).always(cb)
  }
  let template
  const requireTemplate = function (cb) {
    if (template) {
      return cb(template)
    }
    $.ajax({
      'url': 'views/widget.hbs',
      'type': 'get',
      'dataType': 'html'
    }).always(function (res) {
      template = res
      return cb(template)
    })
  }
  const getBatteryStatus = function (cb) {
    Battery.getStatus(function (status, error) {
      if (error) {
        console.error('Battery status not supported')
        return
      }
      cb($.extend({}, status, {
        'charging': !!status.charging,
        'percent': Math.floor(status.level * 100)
      }))
    })
  }
  // FIXME: Demon-stration of callback hell XD
  const weatherReoad = function () {
    weatherRequest(function (res) {
      const data = responseToJSON(res)
      getBatteryStatus(function (battery) {
        requestSunset(function (sun) {
          responsePatcher(data, sun, battery)
          requireTemplate(function (template) {
            renderHandler(template, data)
          })
        })
      })
    })
  }
  const getCustomDate = function () {
    const napok = ['vasárnap', 'hétfő', 'kedd', 'szerda', 'csütörtök', 'péntek', 'szombat']
    const date = new Date()
    return {
      time: date.toString('HH:mm'),
      date: date.toString('yyyy.MM.dd.'),
      day: napok[date.getDay()]
    }
  }
  const updateTime = function () {
    const date = getCustomDate()
    $(window.time).html(date.time)
    $(window.date).html(date.date)
    $(window.day).html(date.day)
  }
  setInterval(updateTime, 10000)
  setInterval(weatherReoad, 60000)
  weatherReoad()
  $('body').on('click', window.reload, weatherReoad)
})(jQuery)
